# Non parametric OD-HMM

This project contains the following files:
+ functions_utiles.R: useful functions for running the EM algorithm
+ functions_utiles_section5_BIC: useful functions for using BIC
+ section5_estimation_duree_graines.R: calculation of the average duration of presence in the seed bank
+ section5_discrimination_s1_BIC.R: study of the discrimination capacity of proba s = 1
+ tests_articles_A2E2.R: all tests with d = 2 and s = 2
    + test_article_A2E2_1.R: test to challenge the algorithm with the first set of initial parameters
    + test_article_A2E2_2.R: test to challenge the algorithm with the second set of initial parameters
    + test_article_A2E2_3.R: test to challenge the algorithm with the third set of initial parameters
    
**To run the functions in this Git repository, you need to have first downloaded and installed the "odhmm" package.** This package is available at the following address : https://forgemia.inra.fr/hanna.bacave/odhmm
