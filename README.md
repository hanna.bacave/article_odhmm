Code to reproduce experimentations of "Non parametric OD-HMM".

Authors : Hanna BACAVE, Pierre-Olivier CHEPTOU, Nikolaos LIMNIOS, Nathalie PEYRARD

2023

**To run the functions in this Git repository, you need to have first downloaded and installed the "odhmm" package.** This package is available at the following address : https://forgemia.inra.fr/hanna.bacave/odhmm
